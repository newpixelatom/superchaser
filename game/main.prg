import "mod_time"
import "mod_map";
import "mod_grproc";
import "mod_key";
import "mod_say";
import "mod_wm";
import "mod_video";
import "mod_scroll";
import "mod_mouse";
import "mod_proc";
import "mod_draw";
import "mod_rand";
import "mod_math";
import "mod_timers";
import "mod_text";
import "mod_screen";
import "mod_string";
import "mod_file";
import "mod_sound";

// opciones de compilacion
global
	bg_fpg;
	enemies_fpg;
	player_fpg;
	misc_fpg;
	stage_speed;
	string game_state = "menu";
	system_white_fnt;
	system_white_small_fnt;
	system_colored_fnt;
	system_colored_small_fnt;
	system_big_title_fnt;

	// punteros a sonidos
	hit_1_wav;
	hit_2_wav;
	hit_3_wav;
	pass_1_wav;
	pass_2_wav;
	pass_3_wav;
	dizzy_wav;
	play_music_loop_ogg;
	menu_music_loop_ogg;
	boss_ogg;
	roar;


	system_title_font;
	monster_fpg;

	int kill_this = 0;
	int current_game = 0;
	new_text_map[10], new_text_focus_map[10];
end
const
	CONTROL_JUMP = 57;
	CONTROL_ESC = 101;

	CONTROL_FIREBALL = 60;
	CONTROL_ICEBALL = 61;
end
local
	float inc_x;
	float inc_y;
end

include "../library/motion_tween.inc";
include "../library/controls.inc";
include "../library/ui/buttons.inc";
include "../library/animation.inc";

local
string status = "falling";
end

include "includes/game.inc";
include "includes/player.inc";
include "includes/monster.inc";
include "includes/menus.inc";
include "includes/flow.inc";

Process Main()
private


Begin

	include "includes/defaultcontrols.inc";
	player_fpg = load_fpg('fpg/player.fpg');
	enemies_fpg = load_fpg('fpg/enemies.fpg');
	monster_fpg = load_fpg('fpg/monster.fpg');
	misc_fpg = load_fpg('fpg/misc.fpg');

	system_white_fnt = fnt_load('fnt/system_white.fnt');
	system_white_small_fnt = fnt_load('fnt/system_white_small.fnt');
	system_colored_fnt = fnt_load('fnt/system_colored.fnt');
	system_colored_small_fnt = fnt_load('fnt/system_colored_small.fnt');
	system_big_title_fnt = fnt_load('fnt/komika_sign.fnt');

	hit_1_wav = load_wav("sound/obstacle-hit1.wav");
	dizzy_wav = load_wav("sound/dizzy-birds.wav");
	hit_2_wav = load_wav("sound/obstacle-hit2.wav");
	hit_3_wav = load_wav("sound/obstacle-hit3.wav");
	pass_1_wav = load_wav("sound/obstacle-passBig1.wav");
	pass_2_wav = load_wav("sound/obstacle-passMed1.wav");
	pass_3_wav = load_wav("sound/obstacle-passSmall1.wav");
	roar = load_wav("sound/Boss_Roar_Big.wav");
	play_music_loop_ogg = load_song("sound/In_Game_Loop1_Master.ogg");
	menu_music_loop_ogg = load_song("sound/Game_Menu_Master.ogg");
	boss_ogg = load_song("sound/Boss_1_What_Do_we_do_now.ogg");

	set_fps(30,0);
	full_screen = true;
	
	
	set_mode(640,360,16);

	loop
		//focus_control();
		if(exists(type game) && game_state != "game")
			signal(type game, S_KILL_TREE);
		end
		switch (game_state):
			case "menu":
				anim_buttons("out");
				start_process();
				current_game = menu();
			end
			case "game":
				anim_buttons("out");
				start_process();
				current_game = game();
			end
			case "story":
				anim_buttons("out");
				start_process();
				current_game = story();
			end
			case "credits":
				anim_buttons("out");
				start_process();
				current_game = credits();
			end
			case "game over":
				start_process();
				current_game = game_over();
			end
			case "win game":
				start_process();
				current_game = win_game();
			end
			case "exit":
				exit();
			end
		end

		Repeat
			
			
		frame;
		Until(!exists(current_game) or current_game == kill_this)
		//current_game = game();
		//while(exists(current_game))
		//	frame;
		//end
	end
End


