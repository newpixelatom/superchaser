global
	Struct menus_items[10]
		text_map;
		text_focus;
		ui_button btn;
	end
	show_menu current_menu;
end
//return last menu item so we keep adding at the end
function menus_next_item()
private 
	var c=0;
	last_item;
begin
	//arranco con el last item -1 despues se incrementa
	last_item = -1;
	for(c=0;c<10;c++)
		if(menus_items[c].text_map!=0)
			last_item = c;	
		end
	end
	return last_item+1;
end

//add menu item. Basically add the btn configuration
function add_menu_btn(string btn_text, btn_font_normal,btn_font_focus)
private
var next_item;
begin
	next_item = menus_next_item();
	//say('add item '+next_item);
	if(next_item<10)
		//say('btn info '+btn_text+' btn_font' + btn_font_normal);
		menus_items[next_item].text_map = write_in_map (btn_font_normal , btn_text ,3);
		menus_items[next_item].text_focus = write_in_map (btn_font_focus , btn_text ,3);
		//create btn
		menus_items[next_item].btn = ui_button(1,menus_items[next_item].text_map,menus_items[next_item].text_focus,menus_items[next_item].text_focus,500,next_item*20,0);
		signal(menus_items[next_item].btn, s_sleep);
		//say('sleep_item '+next_item);
	end
end


//process showing the menu in the middle of the screen
process show_menu(string bg)
public
	
	int anim_running;
private 
	var c=0;
	var pos_x;
	var pos_y;
	largest_size_x=0;
	ancho_pantalla;
	alto_pantalla;
	item_size;
	offset_y;
	type_effect menu_anim_bg;
	int menu_anim_bg_running;
begin
	ancho_pantalla = graphic_info ( 0,BACKGROUND , G_width );
	alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
	x=ancho_pantalla/2;
	y=alto_pantalla/2;
	offset_y=30;
	
	for(c=0;c<10;c++)
		if(exists(menus_items[c].btn))
			//signal(menus_items[c].btn, s_wakeup);
			
			//saco el ancho del grafico del presente proceso
			item_size = graphic_info(0,menus_items[c].btn.graph,G_WIDTH);
			
			pos_x = x - item_size/2;
			if(c==0)
				pos_y = y + menus_items[c].btn.y + offset_y;
			else
				pos_y = menus_items[c-1].btn.y + offset_y;
			end	
			menus_items[c].btn.x = pos_x;
			menus_items[c].btn.y = pos_y;

			//get largest
			if(item_size > largest_size_x)
				largest_size_x = item_size;
			end
			//say('largest_size '+largest_size_x);
		end
	end
	//set size to the menu based on the numbers of buttons and their sizes
	size_x = largest_size_x +50;
	size_y = alto_pantalla;
	graph = map_new(size_x,size_y,16);
	alpha = 70;
	if(bg =='light')
		map_clear(0,graph,rgb(250,250,250,16));
	elseif(bg =='dark')
		map_clear(0,graph,rgb(0,0,0,16));
	end	
	wakeup_menu_items();

//anim menu
	menu_anim_bg.property = &size_x;
	menu_anim_bg.effectType = motion_effect.elasticEaseOut;
	menu_anim_bg.fromValue = 1;
	menu_anim_bg.toValue = graphic_info(0,graph,G_WIDTH);
	menu_anim_bg.duration=50;
	menu_anim_bg_running  = applyEffect(&menu_anim_bg);
	//z = z+5;


	loop
		frame;
	end
	onexit:
		destroy_menu();
end

function wakeup_menu_items()
private 
	var c=0;
begin
	for(c=0;c<10;c++)
		if(menus_items[c].text_map!=0)
			signal(menus_items[c].btn, s_wakeup);
		end
	end
end

function destroy_menu()
private 
	var c=0;
begin
	for(c=0;c<10;c++)
		if(exists(menus_items[c].btn))
			unload_map(0,menus_items[c].text_map);
			menus_items[c].text_map=0;
			unload_map(0,menus_items[c].text_focus);
			if(menus_items[c].btn)
				signal(menus_items[c].btn, S_KILL_TREE);
			end			
		end
	end
end

global
type_effect menu_anim[9];
int menu_anim_running[9];
end

function anim_buttons(string direction)
local
int from_y;
int c=0;
int to_y;
int alto_pantalla;
end
begin
	alto_pantalla = graphic_info ( 0,BACKGROUND , G_height );
	for(c=0;c<10;c++)
		if(exists(menus_items[c].btn))				
				if(direction == 'in')
					from_y = -700+(c*10);
					to_y = menus_items[c].btn.y;
					menu_anim[c].effectType = motion_effect.regularEaseOut;
					menu_anim[c].duration=30-(c*5);
				else
					from_y = menus_items[c].btn.y;
					to_y = alto_pantalla+(c*20);
					menu_anim[c].effectType = motion_effect.regularEaseIn;
					menu_anim[c].duration=20-(c*5);
				end;
				// configuramos una animacion 
				menu_anim[c].property = &menus_items[c].btn.y;
				menu_anim[c].fromValue = from_y;
				menu_anim[c].toValue = to_y;
				menu_anim_running[c]  = applyEffect(&menu_anim[c]);
		end
	end
	//if(direction=="out")
		while (exists(menu_anim_running[0])) 
			if(direction == 'out')
				menus_items[0].btn.alpha-=10;
			end
			frame; 
		end
	//end
end
