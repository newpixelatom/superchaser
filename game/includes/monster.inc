
global
byte anim_run[14] = 1,1,1,1,2,2,2,3,3,3,3,4,4,4,4;
byte anim_stop[39] = 5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9;
byte anim_gas[14] = 10,10,10,11,11,11,12,12,12,13,13,13,14,14,14;
byte anim_baba[13] = 15,15,16,16,17,17,18,18,19,19,20,20,21,21;
byte anim_iced[1] = 22,22;
byte anim_burned[17]= 23,23,23,24,24,24,25,25,25,26,26,26,27,27,27,28,28,28;

end



Process monster()

private
	c_box;
	collided;
	dummy_process d_graph;
	starting_time;
	battle_starting_time;
	
	
Begin
	ctype = c_scroll;
	//set player in the middle
	y= 150;
	x= 0;
	z=40;
	d_graph = dummy_process(monster_fpg,1,0,0);
	status='running';

	set_anim(d_graph,  &anim_run,sizeof(anim_run),true,0,null);

	//collision box
	c_box = map_new(100,100,16);
	map_clear(0,c_box,rgb(255,5,5,16));	
	file = 0;
	graph = c_box;
	alpha=0;
	inc_x =14;
	starting_time = timer[0];
	
	loop

		//gravity	

		switch (status)
			case "running":
				if (current_anim(d_graph)!=&anim_run)
					set_anim(d_graph,  &anim_run,sizeof(anim_run),true,0,null);
				end
				if(timer[0] - starting_time <300)
					inc_x = (timer[0] - starting_time)/15;
				else
					inc_x = 14;
				end		
				
			end
			case "battle":
				
				if (current_anim(d_graph)!=&anim_stop)
					battle_starting_time = timer[0];
					set_anim(d_graph,  &anim_stop,sizeof(anim_stop),false,0,null);
					play_wav ( roar , 0);
					play_song(boss_ogg,-1);					
				end
				inc_x = 0;
			end
			case "gas":
				if (current_anim(d_graph)!=&anim_gas)
					set_anim(d_graph,  &anim_gas,sizeof(anim_gas),false,0,null);					
				end
				inc_x = 0;
			end
			case "burned":
				if (current_anim(d_graph)!=&anim_burned)
					set_anim(d_graph,  &anim_burned,sizeof(anim_burned),false,0,null);					
				end
				inc_x = 0;
			end
			case "baba":
				if (current_anim(d_graph)!=&anim_baba)
					set_anim(d_graph,  &anim_baba,sizeof(anim_baba),false,0,null);					
				end
				inc_x = 0;
			end
			case "iced":
				if (current_anim(d_graph)!=&anim_iced)
					set_anim(d_graph,  &anim_iced,sizeof(anim_iced),false,0,null);					
				end
				inc_x = 0;
			end
		end

		x += inc_x;
		//set limits

		//dummy following the player
		d_graph.x = x;
		d_graph.y = y;
		animate_id(d_graph);

		frame;
	end
	
End
