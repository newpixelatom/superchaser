
global
byte anim_up[15] = 1,1,1,1,2,2,2,3,3,4,5,5,5,6,6,6;
byte anim_still[7] = 5,5,5,5,1,1,1,1;
byte anim_standing[11] = 7,7,7,8,8,8,9,9,9,10,10,10;
byte anim_fireball[11] = 11,11,11,11,12,12,12,13,13,13,13,13;
byte anim_iceball[11] = 14,14,14,14,15,15,15,16,16,16,16,16;
end



Process player()

private
	c_box;
	collided;
	dummy_process d_graph;
	int last_played;
	enemy_passing;
	
Begin
	ctype = c_scroll;
	//set player in the middle
	y= 300;
	x= 200;
	d_graph = dummy_process(player_fpg,1,0,0);

	set_anim(d_graph,  &anim_still,sizeof(anim_still),true,0,null);

	//collision box
	c_box = map_new(50,50,16);
	map_clear(0,c_box,rgb(255,5,5,16));	
	file = 0;
	graph = c_box;
	alpha=0;
	inc_x = 3;

	
	loop

		//gravity	

		switch (status)
			case "dead":
				
				if(y > 300)
					set_anim(d_graph,  &anim_standing,sizeof(anim_standing),true,0,null);
					y = 300;
					
				else
					set_anim(d_graph,  &anim_up,sizeof(anim_up),true,0,null);
				end
			end
			
			case "standing":
				if (current_anim(d_graph)!=&anim_standing)
					set_anim(d_graph,  &anim_standing,sizeof(anim_standing),true,0,null);
				end
				
				if (inc_x > 0)
					inc_x-=1;
				end
				
				if(control(1,CONTROL_JUMP))
					inc_y = -14;
					status = "flap";
					
				end
			end
			case "falling":
				if (current_anim(d_graph)!=&anim_still)
					set_anim(d_graph,  &anim_still,sizeof(anim_still),true,0,null);
				end
				if(control(1,CONTROL_JUMP))
					inc_y = -14;
					status = "flap";
					
				end
			end
			case "flap":
				if (current_anim(d_graph)!=&anim_up)
					set_anim(d_graph,  &anim_up,sizeof(anim_up),false,0,null);
				end
				if (anim_end(d_graph))
					status = "falling";
				end
			end
			case "win":
				set_anim(d_graph,  &anim_up,sizeof(anim_up),true,0,null);
				inc_y = 0;
				if (y != 180)
					inc_y += (180 > y )? 1:-1; 
				end
				inc_x = 0;
			end
			case "battle":
				//if (current_anim(d_graph)!=&anim_up or !d_graph.anim_loop)
				set_anim(d_graph,  &anim_up,sizeof(anim_up),true,0,null);
				//end


				// acomodamos personaje para pelear
				inc_x = 0;
				if (x != pmonster.x - 200)
					inc_x += (pmonster.x - 200 > x )? 1:-1; 
				end

				inc_y = 0;
				if (y != 180)
					inc_y += (180 > y )? 1:-1; 
				end

				if (scroll[0].x0 < (x+100)-320)
					scroll[0].x0 += ((inc_x == 0))?1:inc_x;
				elseif (scroll[0].x0 > (x+100)-320)
					scroll[0].x0 -= 1;
				end


				// una vez acomodado, pasamos a la pelea
				if(x == pmonster.x - 200 && y == 180 && scroll[0].x0 >= (x+100)-320)
					status= "ready_for_battle";
				end

			end
			case "ready_for_battle":
				// mientras dure el indicador de botones se puede disparar algo
				if (get_id(type control_hint))
					if (control(1,CONTROL_FIREBALL))
						status = "fireball";
					end

					if (control(1,CONTROL_ICEBALL))
						status = "iceball";
					end
				end

			end
			case "fireball":
				inc_y = -7;
				// animamos ataque
				if (current_anim(d_graph)!=&anim_fireball)
					set_anim(d_graph,  &anim_fireball,sizeof(anim_fireball),false,0,null);
				end

				// cuando termina ataque chequeamos que pasa
				if (anim_end(d_graph))
					// de ser contra el ataque correcto ganamos
					if (pmonster.status == "gas")
						pmonster.status = "burned";						
						//status = "win";
						status = "battle";
					else
						status = "dead";
					end
				end
			end
			case "iceball":
				inc_y = -7;
				if (current_anim(d_graph)!=&anim_iceball)
					set_anim(d_graph,  &anim_iceball,sizeof(anim_iceball),false,0,null);
				end
				
				if (anim_end(d_graph))
					// de ser contra el ataque correcto ganamos
					if (pmonster.status == "baba")
						pmonster.status = "iced";						
						//status = "win";
						status = "battle";
					else
						status = "dead";
					end
				end
			end
		end

		


		if (status != "battle" 
			&& status != "ready_for_battle"
			&& status != "win")
			inc_y++;
		end
		

		if(inc_y >10)
		 	inc_y =10;
		end

		if(inc_x>=7)
			while(enemy_passing = get_id((type enemy)))
				if(enemy_passing.x - x <200)
					if(enemy_passing.graph ==1 && last_played != enemy_passing.id)
						set_wav_volume(pass_1_wav,70);
						play_wav(pass_1_wav,0);
						last_played = enemy_passing.id;
					end					
					if(enemy_passing.graph ==2  && last_played != enemy_passing.id)
						set_wav_volume(pass_2_wav,70);
						play_wav(pass_2_wav,0);
						last_played = enemy_passing.id;
					end					
					if(enemy_passing.graph ==3  && last_played != enemy_passing.id)
						set_wav_volume(pass_3_wav,70);
						play_wav(pass_3_wav,0);
						last_played = enemy_passing.id;
					end
				end
			end
		end

		if (collided = collision_box(type enemy))

			if (abs(collided.x - x)>abs(collided.y - y) and (collided.x > x))

				if (inc_x>=25 and !get_id(type estrellitas))
					estrellitas();
				else
					if(collided.graph==1 && inc_x>5)
						play_wav(hit_1_wav,0);
					end
					if(collided.graph==2 && inc_x>5)
						play_wav(hit_2_wav,0);
					end
					if(collided.graph==3 && inc_x>5)
						play_wav(hit_3_wav,0);
					end
				end
			end

			while(collision_box(collided))
				if (abs(collided.x - x)>abs(collided.y - y))
					if(collided.x > x)
						x += -1;
						inc_x -= 0.05;

					else
						x += 1;
					end	
				else
					if(collided.y > y)
						y += -1;
						if (status=="falling")
							status = "standing";
						end
					else
						y += 1;
					end
				end
			end
		end

		//set limits

		

		// tope techo
		if(y < 0)
			y = 0;
		end


		// toca el piso
		if(y > 300 and status != "dead")
			status = "standing";
			y = 300;
		end

		if (status != "battle" 
			&& status!="ready_for_battle"
			&& status!="win"
			)

			inc_x += 0.05; // aumenta velocidad progresivamente

		end
		// velocidad de scroll minima
		if(inc_x>2)
			scroll[0].x0 += inc_x*0.9;
		elseif (status != "battle" 
			&& status != "ready_for_battle"
			&& status != "win")
			scroll[0].x0 += 2;
		end

		// limite de velocidad en X
		if(inc_x > 30)
			inc_x = 30;
		end

		// mueve segun velocidad
		y +=inc_y;
		x +=inc_x;

		// limite de posicion en X
		while(x > 360 + scroll[0].x0)
			x-- ; 
		end


		// aca se deberia morir TODO
		if(x < scroll[0].x0 - 60)
			status = "dead";
		end
		//dummy following the player
		d_graph.x = x;
		d_graph.y = y;
		animate_id(d_graph);

		frame;
	end
	onexit:
		unload_map(0,graph);
End


Process enemy()

private
dummy_process enemy;

Begin
	ctype = C_SCROLL;
	
	file = enemies_fpg;
	
	if (rand(0,100)<40)
		graph = rand(3,6);
	
	elseif(rand(0,50)<25)
		graph=2;
	
	else
		graph = 1;
	end

	

	x = scroll[0].x0 + 640 +  graphic_info ( file , graph , g_width );


	y = rand(0,360-graphic_info ( file , graph , g_height ));

	

	loop
		if (x < scroll[0].x0  - 100)
			break;
		end
		if (collision_box(type enemy))
			return;
		end
		frame;
	end

End