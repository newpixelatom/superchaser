global
   int scroll_window;  
   playing;
	pmonster;
end

local
shown = false;
end

process dialog(string text, int delay )
private
	face_id, dialog_id;
	type_effect _anim;
	int anim_running;
	textid;
	shown_at = 0;
	
begin
	shown = false;

	if (delay>0)
		shown_at = timer[8];
		loop
			if (timer[8] - shown_at > delay)
				break;
			end
			frame;
		end
	end

	shown = true;


	// if dialog already present cancel
	while (dialog_id = get_id(type dialog))
		if (dialog_id != id and dialog_id.shown)
			signal(dialog_id, s_kill_tree); 
		end
	end

	face_id = dummy_process(misc_fpg,3,38,360 - 30);
	face_id.ctype = c_screen;

	_anim.property = &face_id.x;
	_anim.effectType = motion_effect.regularEaseOut;
	_anim.fromValue = -77;
	_anim.toValue = 38;
	_anim.duration = 10;
	anim_running  = applyEffect(&_anim);
	
	while(exists(anim_running))
		frame;
	end

	dialog_id = dummy_process(misc_fpg,2,340,360 - 30);
	dialog_id.ctype = c_screen;

	_anim.property = &dialog_id.size;
	_anim.effectType = motion_effect.elasticEaseOut;
	_anim.fromValue = 1;
	_anim.toValue = 100;
	_anim.duration=10;
	anim_running  = applyEffect(&_anim);
	
	while(exists(anim_running))
		frame;
	end

	shown_at = timer[8];

	textid = write(0,340,360 - 30,4,text);

	loop
		if (timer[8] - shown_at > 200)
			break;
		end
		frame;
	end
	delete_text(textid);
	textid = 0;

	_anim.property = &dialog_id.size;
	_anim.effectType = motion_effect.elasticEaseOut;
	_anim.fromValue = 100;
	_anim.toValue = 1;
	_anim.duration=10;
	anim_running  = applyEffect(&_anim);
	
	while(exists(anim_running))
		frame;
	end

	_anim.property = &face_id.x;
	_anim.effectType = motion_effect.regularEaseIn;
	_anim.fromValue = 38;
	_anim.toValue = -77;
	_anim.duration=10;
	anim_running  = applyEffect(&_anim);
	
	while(exists(anim_running))
		frame;
	end
onexit:
	if (textid)
delete_text(textid);
end
	signal(id, s_kill_tree); 
	

end

global
	dialog_near[4] = "I can see him!", "He is pretty close!", "Almost there!", "AHA! There you are!", "Gotcha!";
	dialog_medium[4] = "He is escaping!", "I don't see him anymore!", "Where are you Monster?", "Let's go faster!", "Where is he?";
	dialog_distant[4] = "HURRY UP!", "We are loosing him!!", "He is gone!!", "We're going to loose!", "Come On!";
end

// Main game process!
process game()
private 
	last_appear;
	
	int textid = null;
	waiting;
	starting_time;
	current_effect;
	enemy_passing;
	current_dialog = null;
	wins = 0;
	pressed_action = false;
end
begin
	fade_in_slow();
	play_song ( play_music_loop_ogg , -1);
	bg_fpg = load_fpg('fpg/background.fpg');
	scroll_window = start_scroll(0,bg_fpg,2,1,0,5,0,0);

	scroll[0].x0 = 0;

	playing = player();
	pmonster = monster();
	starting_time = timer[0];

	dialog("Press A button (or SPACE) to fly!",50);
		
	
	dialog("Let's chase that monster!",500);
	
	
	// game loop
	loop


		if (timer[0] - starting_time > 1200 
			and playing.status != "battle" and playing.status != "win"
			and playing.status != "dead")

			if(timer[0] - last_appear > 80 && rand(1,10) == 1)
				enemy();
				last_appear = timer[0];
			end	

			if (pmonster.x - playing.x > 800 and pmonster.x - playing.x < 1000)
				dialog(dialog_near[rand(0,4)],0);
			end

			if (pmonster.x - playing.x > 2700 and pmonster.x - playing.x < 3300)
				dialog(dialog_medium[rand(0,4)],0);
			end

			if (pmonster.x - playing.x > 3800 and pmonster.x - playing.x < 4100)
				
				dialog(dialog_distant[rand(0,4)],0);
			end

			if (pmonster.x - playing.x < 100)

				//mato todos los obstaculos
				while(enemy_passing = get_id((type enemy)))
					signal(enemy_passing, S_KILL_TREE);
				end
				
				pmonster.status = "battle";
				playing.status = "battle";
				//control_hint();
				dialog("What do we do now?!",10);
				current_effect = camera_slowdown();

				while(exists(current_effect))
					frame;
				end

				wins = 0; // contador de batallas ganadas in a row

				while(playing.status != "dead" and playing.status != "win") // entra en la pelea

					if(playing.status=="ready_for_battle")

						pmonster.status = (rand(1,100)<51)?"gas":"baba"; // ataque
						
						// da oportunidad de pegar
						control_hint();
						current_effect = camera_slowdown();

						pressed_action = false;
						while(exists(current_effect)) // esperamos que termine el tiempo para pegar
							if (playing.status == "fireball" or playing.status == "iceball")
								pressed_action = true;
							end
							frame;
						end

						if (!pressed_action)
							playing.status = "dead";
						end

						// o ganamos o perdemos, no?
						if (playing.status != "dead")
							dialog("YEAH!",10);	
							wins++;

							// a las 3 wins ganamos el juego
							if (wins == 3)
								playing.status = "win";
							end
						else

							//dialog("OUCH!",10);
							playing.status = "dead";
							pmonster.status = "running";
						end
					end
					frame;
				end
			end
			
		end

		

		if (pmonster.x - playing.x > 5000)
			playing.status = "dead";
		end
		
		if (playing.status == "dead")
			
			// GAME OVER
			if (textid == null)
			textid = dialog("We've lost the Monster!",10);
			waiting = timer[1];
			end
			if (timer[1] - waiting>300)				
				game_state = 'game over';
				kill_this = id;
			end
		end

		if (playing.status == "win")
			
			// GAME OVER
			if (textid == null)
			textid = dialog("We've defeated the monster!",10);
			waiting = timer[1];
			end
			if (timer[1] - waiting>400)				
				game_state = 'win game';
				kill_this = id;
			end
		end

		if (control(1,CONTROL_ESC))
			game_state = 'menu';
			kill_this = id;
		end

		frame;
		
    end

	
	onexit:
	//set_game_resolution(false); 
	game_state = 'menu';
	kill_this = id;
	//signal(id, s_kill_tree); // mapa el proceso que muestra el scroll
end

// proceso utilizado para mostrar un grafico en pantalla
process dummy_process(file,graph,x,y)
begin
ctype = c_scroll;
	z=father.z-1;
	loop
		frame;
	end
end


process camera_slowdown()
private
	type_effect fps_anim;
	int anim_running;
	int desired_fps;
begin

	fps_anim.property = &desired_fps;
	fps_anim.effectType = motion_effect.regularEaseOut;
	fps_anim.fromValue = 30;
	fps_anim.toValue = 10;
	fps_anim.duration=30;
	anim_running  = applyEffect(&fps_anim);

	 fade ( 150 , 150 , 150 , 3 );

	while(exists(anim_running))
		set_fps(desired_fps,0);
		frame;
	end

	fps_anim.property = &desired_fps;
	fps_anim.effectType = motion_effect.regularEaseIn;
	fps_anim.fromValue = 10;
	fps_anim.toValue = 30;
	fps_anim.duration=30;
	anim_running  = applyEffect(&fps_anim);

	fade ( 100 , 100 , 100 , 3 );
	while(exists(anim_running))
		set_fps(desired_fps,0);
		frame;
	end
onexit:

set_fps(30,0);
fade ( 100 , 100 , 100 , -1 );
	

end

global
	byte anim_estrellitas[31] = 5,5,6,6,7,7,6,6,5,5,6,6,7,7,6,6,5,5,6,6,7,7,6,6,5,5,6,6,7,7,6,6;
end

process estrellitas()
begin

	ctype = c_scroll;
	file = misc_fpg;
	set_anim(id,&anim_estrellitas,sizeof(anim_estrellitas),false,0,null);
	play_wav(dizzy_wav,0);
	while (!anim_end(id))
		animate();
		x = father.x;
		y = father.y;
		z = father.z - 1;
		frame;
	end
end

PROCESS control_hint()
private
	type_effect _anim;
	int anim_running;
	shown_at = 0;
begin

	
	file = misc_fpg;
	graph = 4;

	x = 640 - 70;
	y = 180;

	_anim.property = &size;
	_anim.effectType = motion_effect.elasticEaseOut;
	_anim.fromValue = 1;
	_anim.toValue = 150;
	_anim.duration=40;
	anim_running  = applyEffect(&_anim);
	

	shown_at = timer[5];

	while(exists(anim_running))
		frame;
	end

	loop
		if (control(1,CONTROL_FIREBALL) OR control(1,CONTROL_ICEBALL))
			break;
		end
		if (timer[5] - shown_at > 300)
		break;
		end
		frame;
	end

	_anim.property = &size;
	_anim.effectType = motion_effect.regularEaseOut;
	_anim.fromValue = 100;
	_anim.toValue = 1;
	_anim.duration=30;
	anim_running  = applyEffect(&_anim);

	while(exists(anim_running))
		frame;
	end
end