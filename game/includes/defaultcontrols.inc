controls_map_keyboard(CONTROL_JUMP,1, _SPACE);
controls_map_keyboard(CONTROL_ESC,1, _ESC);
controls_map_keyboard(CONTROL_UP,1, _UP);
controls_map_keyboard(CONTROL_DOWN,1, _DOWN);
controls_map_keyboard(CONTROL_OK,1, _ENTER);

controls_map_keyboard(CONTROL_FIREBALL,1, _CONTROL);
controls_map_keyboard(CONTROL_ICEBALL,1, _ALT);


		controls_map_joy_hat(CONTROL_UP, 1, 0, 0,JOY_HAT_UP);
		controls_map_joy_hat(CONTROL_DOWN, 1, 0, 0,JOY_HAT_DOWN);
		controls_map_joy_hat(CONTROL_LEFT, 1, 0, 0,JOY_HAT_LEFT);
		controls_map_joy_hat(CONTROL_RIGHT, 1, 0, 0,JOY_HAT_RIGHT);
		
		controls_map_joy_axis(CONTROL_UP, 1, 0, 1, -10000);
		controls_map_joy_axis(CONTROL_DOWN, 1, 0, 1, 10000);
		controls_map_joy_axis(CONTROL_LEFT, 1, 0, 0, -10000);
		controls_map_joy_axis(CONTROL_RIGHT, 1, 0, 0, 10000);
		
		controls_map_joystick(CONTROL_ESC,1, 0, 1); 
		controls_map_joystick(CONTROL_JUMP,1, 0, 0); 
		
		controls_map_joystick(CONTROL_OK, 	  1, 0, 0); 
		controls_map_joystick(CONTROL_CANCEL, 1, 0, 1); 

		controls_map_joystick(CONTROL_FIREBALL, 	  1, 0, 3); 
		controls_map_joystick(CONTROL_ICEBALL, 1, 0, 2); 