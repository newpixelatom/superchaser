process menu()
public
	show_menu process_menu;
	int anim_running;
	type_effect anim;
	string menu_bg='dark';
begin

	fade_in_slow();
	// esperamos que termine la animacion
	play_song ( menu_music_loop_ogg , -1);
	x=graphic_info ( 0,BACKGROUND , G_width )/2;
	y=graphic_info ( 0,BACKGROUND , G_height )/2;
	write(system_big_title_fnt,320,90,511,4,"Super Magic");
	write(system_big_title_fnt,320,130,511,4,"Monster Chaser");
	//agrego los botones al menu
	add_menu_btn('START',system_white_fnt,system_colored_fnt);
	add_menu_btn('STORY',system_white_fnt,system_colored_fnt);
	add_menu_btn('CREDITS',system_white_fnt,system_colored_fnt);
	add_menu_btn('EXIT',system_white_fnt,system_colored_fnt);

	//muestro el menu
	process_menu = show_menu(menu_bg);
	z = process_menu.z+1;
	//animo los botones
	anim_buttons('in');
	
	loop
		if(menus_items[0].btn.clicked)
			//say('onclick: '+id);
			game_state = 'game';
			kill_this = id;
			//say('kill this: '+id);
			//say('game_state this: '+game_state);
		end
		if(menus_items[1].btn.clicked)
			game_state = 'story';
			kill_this = id;
		end
		if(menus_items[2].btn.clicked)
			game_state = 'credits';
			kill_this = id;
		end
		if(menus_items[3].btn.clicked)
			game_state = 'exit';
			kill_this = id;
		end
		frame;
	end
end

process start_process()
private
	i;
begin
	fade_out_slow();
	fade_music_off ( 1);
	delete_text(ALL_TEXT);
	if(kill_this != 0)
		for(i = 0; i <= 10; i++)
			unload_map(0,new_text_map[i]);
			unload_map(0,new_text_focus_map[i]);
		end
		signal(kill_this, S_KILL_TREE);
		//say('killed '+kill_this );
		//signal(type item, S_KILL);
		kill_this = 0;		
	end

end

process game_over()
begin

	delete_text(ALL_TEXT);
	fade_in_slow();
	
	write(system_colored_fnt,320,70,511,4,"GAME OVER");
	
	write(system_white_small_fnt,320,130,511,4,"Try again!");
	
	loop

		if (control(1,CONTROL_ESC) or control(1,CONTROL_OK))
			game_state = 'menu';
			kill_this = id;
		end
		frame;
	end

end

process win_game()
begin

	delete_text(ALL_TEXT);
	fade_in_slow();
	
	write(system_colored_fnt,320,70,511,4,"CONGRATULATIONS!!");
	
	write(system_white_small_fnt,320,130,511,4,"You Won the game!!");
	
	write(system_colored_small_fnt,320,150,511,4,"Thank you for playing!!");

	loop

		if (control(1,CONTROL_ESC) or control(1,CONTROL_OK))
			game_state = 'credits';
			kill_this = id;
		end
		frame;
	end

end

process credits()
private
	i;
begin
	
	delete_text(ALL_TEXT);
	fade_in_slow();

	write(system_colored_fnt,320,70,511,4,"GLOBAL GAME JAM 2015");
	write(system_colored_fnt,320,90,511,4,"CORDOBA");
	write(system_colored_small_fnt,320,120,511,4,"ART");
	write(system_white_small_fnt,320,130,511,4,"Roger Arias");
	write(system_white_small_fnt,320,140,511,4,"");
	write(system_colored_small_fnt,320,150,511,4,"PROGRAMMING");
	write(system_white_small_fnt,320,160,511,4,"Javier Arias");
	write(system_white_small_fnt,320,170,511,4,"Lucas Zallio");
	write(system_white_small_fnt,320,180,511,4,"");
	write(system_colored_small_fnt,320,190,511,4,"MUSIC");
	write(system_white_small_fnt,320,200,511,4,"German Martin");
	write(system_colored_small_fnt,320,220,511,4,"POWERED BY");
	write(system_white_small_fnt,320,230,511,4,"BennuGD: http://bennugd.org");
	write(system_white_small_fnt,320,260,511,4,"This game was developed in only 48 hours for the Global Game Jam");
	loop

		if (control(1,CONTROL_ESC) or control(1,CONTROL_OK))
			game_state = 'menu';
			kill_this = id;
		end
		frame;
	end
	

end

process story()
private
	i;
begin
	
	delete_text(ALL_TEXT);
	fade_in_slow();

	write(system_colored_fnt,320,70,511,4,"The legend of Druidy");
	write(system_white_small_fnt,320,120,511,4,"Legend tells one time,");
	write(system_white_small_fnt,320,140,511,4,"a Druid who friends called 'Druidy'");
	write(system_white_small_fnt,320,160,511,4,"Released by mistake a dreadful bean");
	write(system_white_small_fnt,320,180,511,4,"very gross. Trust me, VERY GROSS.");
	write(system_white_small_fnt,320,210,511,4,"So Druidy had to ride his dragon");
	write(system_white_small_fnt,320,230,511,4,"and hunt this thing out");
	write(system_white_small_fnt,320,250,511,4,"before this bean put the world upsidedown");
	write(system_white_small_fnt,320,270,511,4,"Who knows,");
	write(system_white_small_fnt,320,290,511,4,"probably the beast was just misunderstood");
	loop

		if (control(1,CONTROL_ESC) or control(1,CONTROL_OK))
			game_state = 'menu';
			kill_this = id;
		end
		frame;
	end
	

end

function fade_in_slow()
begin
	fade(100,100,100,4); // Fade to normal
	while(fading) frame; end // Wait for the fading to finish
end

function fade_out_slow()
begin
	fade(0,0,0,4); // Fade to black
	while(fading) frame; end // Wait for the fading to finish
end

