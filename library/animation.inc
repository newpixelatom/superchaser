#ifndef __FILE_ANIM
#define __FILE_ANIM

type _anim_x_y
	x = 0;
	y = 0;
end

local 
	// vars usadas por la animacion	
	byte pointer anim_pointer = null;
	_anim_x_y pointer anim_pos_pointer = null; // Esta es el defasaje de la animacion en X e Y
	byte anim_count;
	byte anim_pos = 0;
	bool anim_loop = true;
	bool anim_started = false;
end

function set_anim(process_id,byte pointer p_anim_pointer,p_count,p_anim_loop,p_anim_pos,_anim_x_y pointer p_anim_pos_pointer)
begin
	if(process_id.anim_pointer != p_anim_pointer or p_anim_loop != process_id.anim_loop)
		process_id.anim_pos = p_anim_pos;
		process_id.anim_pointer = p_anim_pointer;
		process_id.anim_count = p_count;
		process_id.anim_loop = p_anim_loop;
		process_id.anim_pos_pointer = p_anim_pos_pointer;
		process_id.anim_started = false;
	end
end

function current_anim(process_id)
begin
	return (process_id.anim_pointer);
end

function anim_set_frame(process_id,pos)
begin	
	process_id.anim_pos = pos;
	process_id.anim_started = false;
end

function anim_end(process_id)
begin
	return process_id.anim_pos >= process_id.anim_count -1;
end

function animate()
begin
    animate_id(father);
end

function animate_id(process_id)
begin
    if (process_id.anim_pointer != null)
            // Cambia el frame de animación 
			if (process_id.anim_started == true)			
				process_id.anim_pos++;
				
				if (process_id.anim_pos >= process_id.anim_count and process_id.anim_loop)
					process_id.anim_pos = 0;
				elseif(process_id.anim_pos >= process_id.anim_count)
					process_id.anim_pos = process_id.anim_count -1;
				end	
			else
				process_id.anim_started = true;
			end
            
            process_id.graph = process_id.anim_pointer[process_id.anim_pos]; // Muestra el grafico correspondiente
            
            if(process_id.anim_pos_pointer != null)
				if(process_id.flags & 1 == 1)
						process_id.x -= process_id.anim_pos_pointer[process_id.anim_pos].x;
				else
						process_id.x += process_id.anim_pos_pointer[process_id.anim_pos].x;
				end
				process_id.y += process_id.anim_pos_pointer[process_id.anim_pos].y;
            end
    end
end

#endif