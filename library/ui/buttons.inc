#ifndef __CONTROLS_LIB
include "../library/controls.inc";
#endif

#ifndef __MOUSE_EVENTS_LIB
include "../library/mouse_events.inc";
#endif


#ifndef __UI_BUTTONS
#define __UI_BUTTONS

Declare Process ui_button(file,normal_graph1,focus_graph1,pressed_graph1,x,y,player_number)
	Public // Declare public variables for the process example_process
		byte clicked = false; // public property to check if the button was pressed
		byte reverse_sound = false; // when true will play sound if the game is muted 
		byte focus = false;
		byte this_player_number;
		normal_graph;
		focus_graph;
		pressed_graph;
	End
End

process ui_button(file,normal_graph1,focus_graph1,pressed_graph1,x,y,player_number)
private
	byte _button_pressed = false;	
	ui_button item, previous_item;
	byte another_have_focus;
	byte assigned;
	byte assign_next;	
begin	

	normal_graph = normal_graph1;
	focus_graph = focus_graph1;
	pressed_graph = pressed_graph1;
	
	z = father.z - 1;
	
	this_player_number = player_number;
	
	priority = father.priority;
	
	loop
		clicked = false;
	
		// si ningun boton en pantalla tiene foco nos asignamos el foco a nosotros mesmos
		if (!focus)
			another_have_focus = false;
			while (item = get_id(type ui_button))
				if (item.focus == true and item.this_player_number == player_number)
					another_have_focus = true;
					break;
				end
			end
			if (!another_have_focus)
				focus = true;
				mouse.x = x;
				mouse.y = y;
			end
		end
		
		// si se aprieta el boton con el mouse
		if (( (mouse_event(MOUSE_DOWN) and collision(type mouse)) or (focus and control(player_number,CONTROL_OK)) ) and !_button_pressed)
			_button_pressed = true;		
			// le choreamos el foco a los otros botones
			if (!focus)
				ui_button_set_focus(id);
			end
		elseif (_button_pressed and ((mouse_event(MOUSE_UP) and !collision(type mouse)) or !focus))
			_button_pressed = false; // si se suelta el mouse afuera del boton se suelta el boton sin disparar evento click
		elseif (mouse_event(MOUSE_DRAG) or mouse_event(MOUSE_DROP))
			_button_pressed = false; // evitamos disparar click si el usuario esta arrastrando la pantalla		
		elseif (_button_pressed and ((mouse_event(MOUSE_UP) and collision(type mouse)) or (focus and control_(player_number,CONTROL_OK))))
			_button_pressed = false;			
			clicked = true;
		elseif(focus) // navegacion por controles
			if (control_(player_number,CONTROL_UP) or control_(player_number,CONTROL_LEFT))
				assigned = false;
				focus = false;
				assign_next = false;
				while (item = get_id(type ui_button))
					if (assign_next and item.this_player_number == player_number)
						ui_button_set_focus(item);
						
						assigned = true;
						break;
					end					
					if (item == id)
						assign_next = true; // marcamos al proximo para que se asigne como en foco
					end
				end
				// si no se asigno a ninguno asignamos al primero de la lista
				if (!assigned)
					frame(0);
					while (item = get_id(type ui_button))
						if (item.this_player_number == player_number)
							ui_button_set_focus(item);
						end
					end		
				end
				
			elseif (control_(player_number,CONTROL_DOWN) or control_(player_number,CONTROL_RIGHT))
				assigned = false;
				focus = false;
				previous_item = null;
				while (item = get_id(type ui_button))
					if (item == id)						
						break;
					end
					if(item.this_player_number == player_number)
						previous_item = item;
					end
				end
				
				if (previous_item == null)
					while (item = get_id(type ui_button))
						if(item.this_player_number == player_number)
							previous_item = item;
						end
							
					end
				end
				
				if (previous_item != null)				
					ui_button_set_focus(previous_item);
				else
					frame(0);
					while (item = get_id(type ui_button))
						if (item.this_player_number == player_number)
							ui_button_set_focus(item);
						end
					end	
				end
			end
		end
		
		// asigna grafico 
		if (!_button_pressed)
			if (focus)
				graph = focus_graph;
			else
				graph = normal_graph;
			end
		else
			graph = pressed_graph;
		end
		
		frame;
	end
end

function ui_button_set_focus(ui_button button_id)
private
ui_button item;
begin
	while (item = get_id(type ui_button))
		if (item.this_player_number == button_id.this_player_number)
			item.focus = false;
		end	
	end			
	button_id.focus = true;	
end

#endif