#ifndef __FILE_LIB
include "../library/file.inc";
#endif

#ifndef __MATH_LIB
include "../library/math.inc";
#endif


#ifndef __TILEDMAP_LIB
#define __TILEDMAP_LIB

#define MAX_TILEMAP_SIZE 1048576

import "mod_scroll";
import "mod_mem"
import "mod_screen"
import "mod_scroll"
import "mod_map"
import "mod_file"
import "mod_regex"

include "../library/tiledmap/source/typedef.inc";
include "../library/tiledmap/source/api.inc";

#endif
