#ifndef __MATH_LIB
#define __MATH_LIB

import "mod_math"

FUNCTION int round ( float f )
//Redondea a las unidades cualquier valor real, probado y funciona con positivos y negativos
PRIVATE
	int n;
	float g;
BEGIN
	g = abs ( f );
	n = g;
	g = g - n;
	IF ( g >= 0.5 )
		IF ( f >= 0 )
			RETURN f + 1;
		ELSE
			RETURN f - 1;
		END
	ELSE
		RETURN f;
	END
END




#endif
