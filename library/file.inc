#ifndef __FILE_LIB
#define __FILE_LIB

import "mod_regex"

function string dirname(string filename)
private
string backup[2];
string match;
begin
    backup[0] = regex_reg[0];
    backup[1] = regex_reg[1];
    backup[2] = regex_reg[2];
    if (regex('^(.*)[/\\]([^/\\]*)$',filename)!=-1)
        match = regex_reg[1];
        regex_reg[0] = backup[0];
        regex_reg[1] = backup[1];
        regex_reg[2] = backup[2];
        return match;
    else
        return "";
    end
end

function string filename(string filename)
private
string backup[2];
string match;
begin   
    backup[0] = regex_reg[0];
    backup[1] = regex_reg[1];
    backup[2] = regex_reg[2];
    if (regex('^(.*)[/\\]([^/\\]*)$',filename)!=-1)
        match = regex_reg[2];
        regex_reg[0] = backup[0];
        regex_reg[1] = backup[1];
        regex_reg[2] = backup[2];
        return match;
    else
        return "";
    end
end



#endif
