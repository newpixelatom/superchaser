/**
 * Control input management library
 *
 */
#ifndef __CONTROLS_LIB
#define __CONTROLS_LIB

import "mod_key"; 
import "mod_joy"; 
import "mod_mem"; 

// default control action constants
const
CONTROL_OK = 1;
CONTROL_CANCEL = 2;
CONTROL_LEFT = 3;
CONTROL_RIGHT = 4;
CONTROL_UP = 5;
CONTROL_DOWN = 6;
end

type controls_buttonmap
	int control = 0; // the control action we want to check
	int player = 0; // the player number (when multiplayer)
	int value = 0; // the key id in the device (keyboard key, joy button, etc)	
	int device_id = -1; // when more than one device conected (joysticks)
	int axisnumber = -1; // when joystick
	int hatnumber = -1; // when joystick
end

global
// controls_defaultjoymap[5] = 9,8,JOY_HAT_LEFT,JOY_HAT_RIGHT,JOY_HAT_UP,JOY_HAT_DOWN;
int controls_mappings_keyboard_count = 0;
controls_buttonmap pointer controls_mappings_keyboard; // list of mapped buttons to the keyboard
int controls_mappings_joystick_count = 0;
controls_buttonmap pointer controls_mappings_joystick; // list of mapped buttons to the joysticks

int pointer controls_disabled = null;
end



function controls_disable(player)
private
	int i;
	elements = 0;
	
begin
	if (controls_disabled!=null)
		elements = (sizeof(controls_disabled)/sizeof(int));
		for(i=0; i<elements; i++)
			if (controls_disabled[i] == player)
				return;
			end
		end
	end
	
	controls_disabled = realloc(controls_disabled,(elements+1)*sizeof(int));
	elements++;
	controls_disabled[elements - 1] = player;
end

function controls_enable(player)
private
	int i;
	elements = 0;
	int pointer new_controls_disabled = null;
begin
	if (controls_disabled != null)		
		elements = (sizeof(controls_disabled)/sizeof(int));
		for(i=0; i<elements; i++)
			if (controls_disabled[i] != player)
				if (new_controls_disabled == null)
					new_controls_disabled = alloc(sizeof(int));
				else
					new_controls_disabled = realloc(new_controls_disabled,(sizeof(new_controls_disabled)/sizeof(int)) + 1);
				end
				controls_disabled[(sizeof(new_controls_disabled)/sizeof(int)) - 1] = controls_disabled[i];
			end
		end
		
		free(controls_disabled);
		controls_disabled = new_controls_disabled;
	end
end

function controls_is_enabled(player)
private
	int i;
	elements = 0;
begin
	if (controls_disabled==null)
		return true;
	end
	
	elements = (sizeof(controls_disabled)/sizeof(int));
	
	for(i=0; i<elements; i++)
		if (controls_disabled[i] == player or controls_disabled[i] == 0)
			return false;
		end
	end	
	
	return true;
end

/**
 * Removes all controls associated to an action
 */
function controls_clear_button(player, button)
private
int i;
controls_buttonmap pointer mappedcontrol_;

controls_buttonmap pointer new_controls_mappings_keyboard;
int new_controls_mappings_keyboard_count = 0;

controls_buttonmap pointer new_controls_mappings_joystick;
int new_controls_mappings_joystick_count = 0;

int _size = 0;
begin
	if (controls_mappings_keyboard != NULL)
		// busca una tecla configurada para el control
		for (i = 0; i < controls_mappings_keyboard_count;i++) 
			mappedcontrol_ = controls_mappings_keyboard[i];
			if (mappedcontrol_.control == button and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
				continue; // si es del player y el boton indicado no lo pasamos al nuevo puntero				
			else	
				// calcula el nuevo tamaño
				if ((new_controls_mappings_keyboard) == NULL)
					_size = sizeof(controls_buttonmap);
				else
					_size = (new_controls_mappings_keyboard_count+1) *  sizeof(controls_buttonmap);
				end
				
				// redimencionar tamaño del puntero
				new_controls_mappings_keyboard = realloc (new_controls_mappings_keyboard, _size);
				
				new_controls_mappings_keyboard_count++;
				
				// setear el nuevo elemento	
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].control = mappedcontrol_.control;
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].player = mappedcontrol_.player;
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].value = mappedcontrol_.value;				
			end
		end	
		
		free(controls_mappings_keyboard); // libera mem del puntero viejo
		
		controls_mappings_keyboard = new_controls_mappings_keyboard;
		controls_mappings_keyboard_count = new_controls_mappings_keyboard_count;		
	end
	
	_size = 0;
	
	if (controls_mappings_joystick != NULL)
		// busca una tecla configurada para el control
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
				continue; // si es del player y el boton indicado no lo pasamos al nuevo puntero				
			else	
				// calcula el nuevo tamaño
				if ((new_controls_mappings_joystick) == NULL)
					_size = sizeof(controls_buttonmap);
				else
					_size = (new_controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
				end
				
				// redimencionar tamaño del puntero
				new_controls_mappings_joystick = realloc (new_controls_mappings_joystick, _size);
				
				new_controls_mappings_joystick_count++;
				
				// setear el nuevo elemento	
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].control = mappedcontrol_.control;
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].player = mappedcontrol_.player;
				new_controls_mappings_joystick[new_controls_mappings_joystick_count - 1].value = mappedcontrol_.value;
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].device_id = mappedcontrol_.device_id;
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].axisnumber = mappedcontrol_.axisnumber;
				new_controls_mappings_keyboard[new_controls_mappings_keyboard_count - 1].hatnumber = mappedcontrol_.hatnumber;
			end
		end	
		
		free(controls_mappings_joystick); // libera mem del puntero viejo
		
		controls_mappings_joystick = new_controls_mappings_joystick;
		controls_mappings_joystick_count = new_controls_mappings_joystick_count;		
	end
	
	
	return;
end

/**
 * Maps a keyboard scancode to a game control action.
 */
function controls_map_keyboard(int control,int player, int scancode)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_keyboard) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_keyboard_count;i++) 
			mappedcontrol_ = controls_mappings_keyboard[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.value == scancode)			
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_keyboard_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_keyboard = realloc (controls_mappings_keyboard, _size);
	
	controls_mappings_keyboard_count++;
	
	// setear el nuevo elemento	
	controls_mappings_keyboard[i].control = control;
	controls_mappings_keyboard[i].player = player;
	controls_mappings_keyboard[i].value = scancode;
end
/**
 * Maps a joystick axis to an action
 */
function controls_map_joy_hat(int control, int player, int joystick_id, int hatnumber, int value)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_joystick) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if (mappedcontrol_.control == (control  - 10000)  and 
				mappedcontrol_.player == player and 
				mappedcontrol_.hatnumber == hatnumber and
				mappedcontrol_.value == value )
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
	
	controls_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	controls_mappings_joystick[i].control = (control  - 10000);
	controls_mappings_joystick[i].device_id = joystick_id;
	controls_mappings_joystick[i].player = player;
	controls_mappings_joystick[i].hatnumber = hatnumber;
	controls_mappings_joystick[i].axisnumber = -1;
	controls_mappings_joystick[i].value = value ;
end
/**
 * Maps a joystick axis to an action
 */
function controls_map_joy_axis(int control, int player, int joystick_id, int axisnumber, int value)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_joystick) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.axisnumber == axisnumber and
				mappedcontrol_.value == value)
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
	
	controls_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	controls_mappings_joystick[i].control = control;
	controls_mappings_joystick[i].device_id = joystick_id;
	controls_mappings_joystick[i].player = player;
	controls_mappings_joystick[i].axisnumber = axisnumber;
	controls_mappings_joystick[i].hatnumber = -1;
	controls_mappings_joystick[i].value = value;
end

/**
 * Maps a joystick button to a game control action.
 */
function controls_map_joystick(int control, int player, int joystick_id, int button)
private
int i;
controls_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((controls_mappings_joystick) == NULL)
		_size = sizeof(controls_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < controls_mappings_joystick_count;i++) 
			mappedcontrol_ = controls_mappings_joystick[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.device_id == joystick_id and 
				mappedcontrol_.value == button)
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (controls_mappings_joystick_count+1) *  sizeof(controls_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	controls_mappings_joystick = realloc (controls_mappings_joystick, _size);
	
	controls_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	controls_mappings_joystick[i].control = control;
	controls_mappings_joystick[i].device_id = joystick_id;
	controls_mappings_joystick[i].player = player;
	controls_mappings_joystick[i].value = button;
	controls_mappings_joystick[i].axisnumber = -1;
	controls_mappings_joystick[i].hatnumber = -1;
end


 
/**
 * Check if a button is pressed via the keyboard handler
 */
function controls_button_checkkeyb(player, button)
private
int i;
controls_buttonmap pointer mappedcontrol_;
begin
	// busca una tecla configurada para el control
	for (i = 0; i < controls_mappings_keyboard_count;i++) 
		mappedcontrol_ = controls_mappings_keyboard[i];
		if (mappedcontrol_.control == button and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)))			
			
			if (key(mappedcontrol_.value))
				return true;
			end
		end
	end
	
	return false;
end

/**
 * returns true if a button on any device binded to an action is pressed. 
 * if the device button is an axis it will return the value of the axis
 */
function controls_button_checkjoy(player, button)
private
int i;
controls_buttonmap pointer mappedcontrol_;
value;
begin
	// busca una tecla configurada para el control
	for (i = 0; i < controls_mappings_joystick_count;i++) 
		mappedcontrol_ = controls_mappings_joystick[i];
		
		if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)) and mappedcontrol_.device_id > -1)
			
			// check joystick
			
			
			if(mappedcontrol_.axisnumber > -1 and mappedcontrol_.control == button )
				if (mappedcontrol_.value > 0)
					if((value = joy_getaxis(mappedcontrol_.device_id,mappedcontrol_.axisnumber)) > mappedcontrol_.value)
						return value;
					end
				else
					if((value = joy_getaxis(mappedcontrol_.device_id,mappedcontrol_.axisnumber)) < mappedcontrol_.value)
						return value;
					end
				end
			end
			if(mappedcontrol_.hatnumber > -1 and mappedcontrol_.control == (button  - 10000))
				// HAT check
				if (joy_gethat(mappedcontrol_.device_id ,0) == mappedcontrol_.value)
					return true;
				end
			end
			if (joy_getbutton(mappedcontrol_.device_id ,mappedcontrol_.value) and mappedcontrol_.control == button)
				return true;
			end
			
			// •  •  •  •  •  •  •  • 
			
		end
	end
	
	return false;
end

/**
 * Check for if a control button is pressed in any controller device
 */
function control(player, button)
private
	value;
begin
	if (!controls_is_enabled(player))
		return false;
	end
	// check keyboard
	if (controls_button_checkkeyb(player, button))
		return true;
	end
	// check joystick
    if ((value = controls_button_checkjoy(player, button)) != false )
		return value;
	end
	return false;
end

/**
 * Check the button is pressed and then released
 */ 
function control_(player, button)
private
    value, return_value = false;	
begin
    while((value = control(player, button)) != false)		
		return_value = value;
        frame; // limpiamos buffer de teclas 
    end

    return return_value;
end

#endif