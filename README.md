# README #

This game was built with BennuGD: [http://bennugd.org](http://bennugd.org). It needs last version for be compiled.

### What is this repository for? ###

* This repo was created for the Global Game Jam 2015, in Córdoba, Argentina

### Contribution guidelines ###

* Music by German Martín
* Programming by Javier Arias and Lucas Zallio
* Art by Roger Arias

### Who do I talk to? ###

* Repo owner or admin